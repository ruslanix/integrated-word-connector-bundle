<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\WordConnectorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Persistence\ObjectRepository;

class WordUploadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'contentType',
            'document',
            array(
                'class' => "Integrated\Bundle\ContentBundle\Document\ContentType\ContentType",
                'query_builder' => function(ObjectRepository $or) {
                    return $or->createQueryBuilder('o')
                        ->field('class')->equals('Integrated\\Bundle\\ContentBundle\\Document\\Content\\Article')
                        ->sort('class', 'asc')
                        ->sort('type', 'asc');
                },
                'label' => 'Article type'
            )
        );

        $builder->add('document', 'file');
        
        $builder->add('submit', 'submit', array('label' => 'Upload'));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'word_upload';
    }
}