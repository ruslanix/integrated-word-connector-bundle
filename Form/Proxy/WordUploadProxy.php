<?php

namespace Integrated\Bundle\WordConnectorBundle\Form\Proxy;

class WordUploadProxy
{
    protected $contentType = null;

    protected $document = null;

    public function getContentType()
    {
        return $this->contentType;
    }

    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    public function getDocument()
    {
        return $this->document;
    }

    public function setDocument($document)
    {
        $this->document = $document;
    }
}