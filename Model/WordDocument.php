<?php

namespace Integrated\Bundle\WordConnectorBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class WordDocument
{
    protected $title;

    protected $author;

    protected $metadata;

    protected $xhtmlMainContent;

    protected $images;

    public function __construct()
    {
        $this->metadata = new ArrayCollection();
        $this->images = array();
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function addImage(WordDocumentImage $image)
    {
        $this->images[] = $image;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setMetadata(ArrayCollection $metadata)
    {
        $this->metadata = $metadata;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }
    
    public function setXhtmlMainContent($xhtmlMainContent)
    {
        $this->xhtmlMainContent = $xhtmlMainContent;
    }

    public function getXhtmlMainContent()
    {
        return $this->xhtmlMainContent;
    }
}
