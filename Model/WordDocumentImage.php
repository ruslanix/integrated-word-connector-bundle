<?php

namespace Integrated\Bundle\WordConnectorBundle\Model;

class WordDocumentImage
{
    protected $fileName;

    protected $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
}
