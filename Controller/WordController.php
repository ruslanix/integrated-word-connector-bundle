<?php

namespace Integrated\Bundle\WordConnectorBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Integrated\Bundle\WordConnectorBundle\Form\Proxy\WordUploadProxy;
use Integrated\Bundle\WordConnectorBundle\Form\Type\WordUploadType;
use Integrated\Bundle\ContentBundle\Document\Content\Article;
use Integrated\Bundle\ContentBundle\Document\Content\File;

class WordController extends Controller
{
	/**
	 * @Template()
	 * @return array
	 */
    public function newAction(Request $request)
    {
        $form = $this->createForm(new WordUploadType(), new WordUploadProxy());

        $form->handleRequest($request);
        
        if ($form->isValid()) {

            $uploadedFile = $form->getData()->getDocument();
            $contentType = $form->getData()->getContentType();

            /* @type Integrated\Bundle\WordConnectorBundle\Model\WordDocument */
            $wordDocument = $this->get("integrated_word_connector.word_parser")
                ->parseUploadedFile($uploadedFile);

            //TODO: move this logic to some WordDocument2Content service
            $article = new Article();
            $article->setContentType($contentType->getType());
            $article->setTitle($wordDocument->getTitle());
            $article->setContent($wordDocument->getXhtmlMainContent());

            $dm = $this->get('doctrine_mongodb')->getManager();

//            $references = new ArrayCollection();
//            foreach($wordDocument->getImages() as $image)
//            {
//                $file = new File();
//                $file->setFile($image->getFileName());
//                $file->setName($image->getTitle());
//
//                $dm->persist($file);
//
//                $references->add($file);
//            }
//
//            $article->setReferences($references);

            $dm->persist($article);
            $dm->flush();

            // Set flash message
            $this->get('braincrafted_bootstrap.flash')->success(sprintf('A new %s is created', $contentType->getName()));

            //TODO: improve this. JSM and JvL are gonna kick me if they see this
            $indexer = $this->get('integrated_solr.indexer');
            $indexer->execute();
            file_get_contents('http://' . $this->container->getParameter('solr_host') . ':' . $this->container->getParameter('solr_port') . '/solr/' . $this->container->getParameter('solr_core') . '/update?commit=true');

            return $this->redirect($this->generateUrl('integrated_content_content_edit', array('id' => $article->getId())));
        }

        return array(
            'form' => $form->createView()
        );
    }
}
