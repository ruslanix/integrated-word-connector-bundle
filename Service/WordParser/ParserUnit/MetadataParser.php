<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser\ParserUnit;

use Doctrine\Common\Collections\ArrayCollection;

use Ruslanix\CommandChain\CommandUnit\BaseCommandUnit;
use Ruslanix\CommandChain\ContextContainer\ContextContainer;

class MetadataParser extends BaseCommandUnit
{
    public function process(ContextContainer $context)
    {
        $metadata = new ArrayCollection();

        $xmlContent = $context->getOrException('xmlContent');
        if (!$xmlContent) {
            return;
        }
        
        $dom = new \DomDocument('1.0');
        $dom->loadXML($xmlContent);
        $metas = $dom->getElementsByTagName('meta');
        if ($metas) {
            foreach ($metas as $meta) {
                $metadata->set($meta->getAttribute('name'), $meta->getAttribute('content'));
            }
        }

        $context->getOrException('wordDocument')->setMetadata($metadata);
    }
}