<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser\ParserUnit;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;

use Ruslanix\CommandChain\CommandUnit\BaseCommandUnit;
use Ruslanix\CommandChain\ContextContainer\ContextContainer;
use Integrated\Bundle\WordConnectorBundle\Model\WordDocumentImage;
use Integrated\Bundle\WordConnectorBundle\Model\WordDocument;

class ImagesParser extends BaseCommandUnit
{
    /**
     *
     * @var Fileinfo resource
     */
    protected $finfo;
    protected $uploadDir;

    public function __construct($uploadDir)
    {
        $this->uploadDir = $uploadDir;
        $this->finfo = finfo_open(FILEINFO_MIME_TYPE);
    }

    public function __destruct()
    {
        finfo_close($this->finfo);
    }

    public function process(ContextContainer $context)
    {
        $attachmentsDir = $context->getOrException('attachmentsDir');
        $wordDocument = $context->getOrException('wordDocument');

        $finder = $this->getConfiguredFinder($attachmentsDir);
        $this->saveImages($finder, $wordDocument);
    }

    protected function getConfiguredFinder($attachmentsDir)
    {
        $finder = new Finder();

        $finder
            ->files()
            ->in($attachmentsDir)
            ->filter(function(\SplFileInfo $file) {

                $mimeType = finfo_file($this->finfo, $file->getPathname());
                list($baseType) = explode("/", $mimeType);

                return $baseType == "image";
            });

        return $finder;
    }

    protected function saveImages($finder, WordDocument $wordDocument)
    {
        $webAttachmentsDir = $this->uploadDir . "/word-connector/".mt_rand();

        $fs = new Filesystem();
        $createDir = true;

        foreach ($finder as $file) {

            if ($createDir) {
                $fs->mkdir($webAttachmentsDir);
                $createDir = false;
            }
            
            $imageNameWithoutExt = pathinfo($file->getPathname(), PATHINFO_FILENAME);
            $imageNameWithExt = pathinfo($file->getPathname(), PATHINFO_BASENAME);
            $imagePath = $webAttachmentsDir . '/' . $imageNameWithExt;

            $fs->copy($file->getPathname(), $imagePath);

            $image = new WordDocumentImage();
            $image->setFileName($imagePath);
            $image->setTitle(pathinfo($imageNameWithoutExt));

            $wordDocument->addImage($image);
        }
    }
}