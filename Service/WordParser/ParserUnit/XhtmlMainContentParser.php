<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser\ParserUnit;

use Ruslanix\CommandChain\CommandUnit\BaseCommandUnit;
use Ruslanix\CommandChain\ContextContainer\ContextContainer;

class XhtmlMainContentParser extends BaseCommandUnit
{
    public function process(ContextContainer $context)
    {
        $xhtmlMainContent = "";

        $xmlContent = $context->getOrException('xmlContent');
        if (!$xmlContent) {
            return;
        }

        $dom = new \DomDocument('1.0');
        $dom->loadXML($xmlContent);
        
        $body = $dom->getElementsByTagName('body');

        if ($body) {
            
            foreach ($body->item(0)->childNodes as $node) {
                $xhtmlMainContent .= $dom->saveXML($node);
            }

            $xhtmlMainContent = trim($xhtmlMainContent);
        }

        $context->getOrException('wordDocument')->setXhtmlMainContent($xhtmlMainContent);
     }
}