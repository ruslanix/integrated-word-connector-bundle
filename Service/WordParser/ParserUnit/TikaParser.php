<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser\ParserUnit;

use Symfony\Component\Filesystem\Filesystem;

use Ruslanix\CommandChain\CommandUnit\BaseCommandUnit;
use Ruslanix\CommandChain\ContextContainer\ContextContainer;
use Integrated\Bundle\WordConnectorBundle\Service\TikaClient\TikaClient;

class TikaParser extends BaseCommandUnit
{
    /**
     *
     * @var TikaClient
     */
    protected $tikaClient;

    public function __construct(TikaClient $tikaClient)
    {
        $this->tikaClient = $tikaClient;
    }

    public function process(ContextContainer $context)
    {
        $uploadedFilePath = $context->getOrException("uploadedFilePath");
        $attachmentsDir = $this->createTempDir();

        $xmlContent = $this->tikaClient
            ->withExtractAttachmentsTo($attachmentsDir)
            ->process($uploadedFilePath);

        if (!$xmlContent) {
            throw new \Exception(sprintf("[TikaParser] tika client return empty result"));
        }

        $context->set('xmlContent', $xmlContent);
        $context->set('attachmentsDir', $attachmentsDir);
    }

    protected function createTempDir()
    {
        $dir = sys_get_temp_dir() . '/integrated/' . mt_rand();

        $fs = new Filesystem();
        $fs->mkdir($dir);

        return $dir;
    }
}