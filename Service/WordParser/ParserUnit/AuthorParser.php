<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser\ParserUnit;

use Ruslanix\CommandChain\CommandUnit\BaseCommandUnit;
use Ruslanix\CommandChain\ContextContainer\ContextContainer;

class AuthorParser extends BaseCommandUnit
{
    public function process(ContextContainer $context)
    {
        $author = "";

        $wordDocument = $context->getOrException('wordDocument');
        $metadata = $wordDocument->getMetadata();

        $authorKeys = array(
            'Author',
            'meta:author',
            'Last-Author',
            'meta:last-author'
        );

        foreach ($authorKeys as $key) {
            if ($metadata->containsKey($key)) {
                $author = $metadata->get($key);
                break;
            }
        }

        $wordDocument->setAuthor($author);
    }
}