<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser\ParserUnit;

use Ruslanix\CommandChain\CommandUnit\BaseCommandUnit;
use Ruslanix\CommandChain\ContextContainer\ContextContainer;

class TitleParser extends BaseCommandUnit
{
    public function process(ContextContainer $context)
    {
        $title = $context->getOrException('clientOriginalName');

        $wordDocument = $context->getOrException('wordDocument');

        if ($wordDocument->getXhtmlMainContent()) {
            $dom = new \DomDocument('1.0');

            $dom->loadXML("<body>" . trim($wordDocument->getXhtmlMainContent())."</body>");

            $bodyTag = $dom->firstChild;
            $firstTag = $bodyTag->firstChild;

            if ($firstTag && $this->isHeaderTag($firstTag)) {
                $title = trim($firstTag->nodeValue);
            }
        }

        $wordDocument->setTitle($title);
    }

    protected function isHeaderTag(\DOMNode $node)
    {
        // very simple check
        return in_array($node->nodeName, array('h1', 'h2', 'h3', 'h4'));
    }
}