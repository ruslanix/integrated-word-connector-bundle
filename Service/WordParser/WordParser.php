<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\WordParser;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Ruslanix\CommandChain\Chain\SimpleChain;
use Integrated\Bundle\WordConnectorBundle\Service\TikaClient\TikaClient;
use Integrated\Bundle\WordConnectorBundle\Model\WordDocument;

class WordParser
{
    /**
     *
     * @var TikaClient
     */
    protected $tikaClient;

    /**
     *
     * @var Directory path where to store attachments
     */
    protected $uploadDir;

    public function __construct(TikaClient $tikaClient, $uploadDir)
    {
        $this->tikaClient = $tikaClient;
        $this->uploadDir = $uploadDir;
    }

    public function parseUploadedFile(UploadedFile $file)
    {
        $wordDocument = new WordDocument();

        $this->getConfiguredParserChain($file->getPathname(), $file->getClientOriginalName(), $wordDocument)
            ->process();

        return $wordDocument;
    }

    protected function getConfiguredParserChain($uploadedFilePath, $clientOriginalName, WordDocument $wordDocument)
    {
        $chain = new SimpleChain();

        $chain->getContextContainer()->set('clientOriginalName', $clientOriginalName);
        $chain->getContextContainer()->set('uploadedFilePath', $uploadedFilePath);
        $chain->getContextContainer()->set('wordDocument', $wordDocument);

        $chain->addCommandUnit(new ParserUnit\TikaParser($this->tikaClient));
        $chain->addCommandUnit(new ParserUnit\MetadataParser());
        $chain->addCommandUnit(new ParserUnit\XhtmlMainContentParser());
        $chain->addCommandUnit(new ParserUnit\AuthorParser());
        $chain->addCommandUnit(new ParserUnit\TitleParser());
        $chain->addCommandUnit(new ParserUnit\ImagesParser($this->uploadDir));

        return $chain;
    }
}