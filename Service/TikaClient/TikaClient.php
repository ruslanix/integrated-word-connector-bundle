<?php

namespace Integrated\Bundle\WordConnectorBundle\Service\TikaClient;

class TikaClient
{
    /**
     * Path to tika jar or url
     * 
     * @var string
     */
    protected $tikaPath;

    /**
     * directory path where to extract attachments
     *
     * @var string
     */
    protected $extractAttachmentsTo = null;
    
    /**
     * Vendor TikaClient from marcelomx/tika-client package
     * 
     * @var \TikaClient
     */
    protected $wrappedClient = null;

    public function __construct($tikaPath)
    {
        $this->tikaPath = $tikaPath;
    }

    public function withExtractAttachmentsTo($path)
    {
        if (!is_writable($path)) {
            throw new \InvalidArgumentException(sprintf('[TikaClient] The path %s is not writable', $path));
        }

        $this->extractAttachmentsTo = $path;

        return $this;
    }

    /**
     * Execute tika parsing with configured options
     *
     * @param string $documentPath
     */
    public function process($documentPath)
    {
        if ($this->extractAttachmentsTo) {
            $this->getWrappedClient()->extract($documentPath, $this->extractAttachmentsTo);
        }

        return $this->getWrappedClient()->getXhtml($documentPath);
    }

    /**
     * Return vendor TikaClient from marcelomx/tika-client package
     */
    protected function getWrappedClient()
    {
        if (!$this->wrappedClient) {
            $this->wrappedClient = new \TikaClient($this->tikaPath);
        }

        return $this->wrappedClient;
    }
}